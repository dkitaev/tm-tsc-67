package ru.tsc.kitaev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.tsc.kitaev.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/projects")
public interface IProjectEndpoint {

    @NotNull
    @WebMethod
    @PostMapping("/add")
    Project add(
            @WebParam(name = "project", partName = "project")
            @NotNull @RequestBody final Project project
    );

    @NotNull
    @WebMethod
    @PutMapping("/save")
    Project save(
            @WebParam(name = "project", partName = "project")
            @NotNull @RequestBody final Project project
    );

    @Nullable
    @WebMethod
    @GetMapping("/findAll")
    List<Project> findAll();

    @Nullable
    @WebMethod
    @GetMapping("/findById/{id}")
    Project findById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable(value = "id") final String id
    );

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "id", partName = "id")
            @Nullable @PathVariable(value = "id") final String id
    );

    @WebMethod
    @GetMapping("/count")
    int getSize();

    @WebMethod
    @DeleteMapping("/clear")
    void clear();

    @WebMethod
    @DeleteMapping("/delete")
    void delete(
            @WebParam(name = "project", partName = "project")
            @NotNull @RequestBody final Project project
    );

    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable(value = "id") final String id
    );

}
