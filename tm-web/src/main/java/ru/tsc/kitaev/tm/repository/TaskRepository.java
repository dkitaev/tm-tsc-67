package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.kitaev.tm.model.Task;

import java.util.List;

@Repository
public interface TaskRepository extends AbstractRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull String projectId);

}
