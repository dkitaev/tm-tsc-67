package ru.tsc.kitaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.kitaev.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.kitaev.tm.api.service.ITaskService;
import ru.tsc.kitaev.tm.client.TaskRestEndpointClient;
import ru.tsc.kitaev.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.tsc.kitaev.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpointImpl implements ITaskEndpoint {

    @NotNull
    @Autowired
    private ITaskService taskService;

    @Override
    @NotNull
    @WebMethod
    @PostMapping("/add")
    public Task add(
            @WebParam(name = "task", partName = "task")
            @NotNull @RequestBody final Task task
    ) {
        return taskService.add(task);
    }

    @Override
    @NotNull
    @WebMethod
    @PutMapping("/save")
    public Task save(
            @WebParam(name = "task", partName = "task")
            @NotNull @RequestBody final Task task
    ) {
        return taskService.update(task);
    }

    @Override
    @Nullable
    @WebMethod
    @GetMapping("/findAll")
    public List<Task> findAll() {
        return taskService.findAll();
    }

    @Override
    @Nullable
    @WebMethod
    @GetMapping("/findById/{id}")
    public Task findById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable(value = "id") final String id
    ) {
        return taskService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @WebParam(name = "id", partName = "id")
            @Nullable @PathVariable(value = "id") final String id
    ) {
        return taskService.existsById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public int getSize() {
        return taskService.getSize();
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        taskService.clear();
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete")
    public void delete(
            @WebParam(name = "task", partName = "task")
            @NotNull @RequestBody final Task task
    ) {
        taskService.delete(task);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable(value = "id") final String id
    ) {
        taskService.deleteById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAllByProjectId/{projectId}")
    public List<Task> findAllByProjectId(
            @WebParam(name = "projectId", partName = "projectId")
            @Nullable @PathVariable(value = "projectId") final String projectId
    ) {
        return taskService.findAllByProjectId(projectId);
    }

}
