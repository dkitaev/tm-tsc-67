package ru.tsc.kitaev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.tsc.kitaev.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/tasks")
public interface ITaskEndpoint {

    @NotNull
    @WebMethod
    @PostMapping("/add")
    Task add(
            @WebParam(name = "task", partName = "task")
            @NotNull @RequestBody final Task task
    );

    @NotNull
    @WebMethod
    @PutMapping("/save")
    Task save(
            @WebParam(name = "task", partName = "task")
            @NotNull @RequestBody final Task task
    );

    @Nullable
    @WebMethod
    @GetMapping("/findAll")
    List<Task> findAll();

    @Nullable
    @WebMethod
    @GetMapping("/findById/{id}")
    Task findById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable(value = "id") final String id
    );

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "id", partName = "id")
            @Nullable @PathVariable(value = "id") final String id
    );

    @WebMethod
    @GetMapping("/count")
    int getSize();

    @WebMethod
    @DeleteMapping("/clear")
    void clear();

    @WebMethod
    @DeleteMapping("/delete")
    void delete(
            @WebParam(name = "task", partName = "task")
            @NotNull @RequestBody final Task task
    );

    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable(value = "id") final String id
    );

    @WebMethod
    @GetMapping("/findAllByProjectId/{projectId}")
    List<Task> findAllByProjectId(
            @WebParam(name = "projectId", partName = "projectId")
            @Nullable @PathVariable(value = "projectId") final String projectId
    );

}
