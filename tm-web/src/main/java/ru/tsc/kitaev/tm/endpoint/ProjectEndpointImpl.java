package ru.tsc.kitaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.kitaev.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.kitaev.tm.api.service.IProjectService;
import ru.tsc.kitaev.tm.client.ProjectRestEndpointClient;
import ru.tsc.kitaev.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.tsc.kitaev.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpointImpl implements IProjectEndpoint {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Override
    @NotNull
    @WebMethod
    @PostMapping("/add")
    public Project add(
            @WebParam(name = "project", partName = "project")
            @NotNull @RequestBody final Project project
    ) {
        return projectService.add(project);
    }

    @Override
    @NotNull
    @WebMethod
    @PutMapping("/save")
    public Project save(
            @WebParam(name = "project", partName = "project")
            @NotNull @RequestBody final Project project
    ) {
        return projectService.update(project);
    }

    @Override
    @Nullable
    @WebMethod
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return projectService.findAll();
    }

    @Override
    @Nullable
    @WebMethod
    @GetMapping("/findById/{id}")
    public Project findById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable(value = "id") final String id
    ) {
        return projectService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @WebParam(name = "id", partName = "id")
            @Nullable @PathVariable(value = "id") final String id
    ) {
        return projectService.existsById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public int getSize() {
        return projectService.getSize();
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        projectService.clear();
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete")
    public void delete(
            @WebParam(name = "project", partName = "project")
            @NotNull @RequestBody final Project project
    ) {
        projectService.delete(project);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable(value = "id") final String id
    ) {
        projectService.deleteById(id);
    }

}
