package ru.tsc.kitaev.tm.service;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tsc.kitaev.tm.api.service.IProjectService;
import ru.tsc.kitaev.tm.model.Project;

@Service
@NoArgsConstructor
public final class ProjectService extends AbstractService<Project> implements IProjectService {

}
