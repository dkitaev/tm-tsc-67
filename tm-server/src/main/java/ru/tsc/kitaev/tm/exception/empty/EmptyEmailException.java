package ru.tsc.kitaev.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.exception.AbstractException;

public final class EmptyEmailException extends AbstractException {

    public EmptyEmailException() {
        super("Error. Email is empty.");
    }

    public EmptyEmailException(@NotNull String value) {
        super("Error" + value + " Email is empty.");
    }

}
