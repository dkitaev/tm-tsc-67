package ru.tsc.kitaev.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kitaev.tm.api.service.model.IProjectTaskService;
import ru.tsc.kitaev.tm.exception.empty.EmptyIndexException;
import ru.tsc.kitaev.tm.exception.empty.EmptyUserIdException;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.Task;
import ru.tsc.kitaev.tm.repository.model.ProjectRepository;
import ru.tsc.kitaev.tm.repository.model.TaskRepository;

import java.util.List;

@Service
@AllArgsConstructor
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    public ProjectRepository projectRepository;

    @NotNull
    @Autowired
    public TaskRepository taskRepository;

    @NotNull
    @Override
    public List<Task> findTaskByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        return taskRepository.findAllByUserIdAndProjectId(userId, projectId);
    }

    @Override
    @Transactional
    public void bindTaskById(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        @NotNull final Task task = taskRepository.findByUserIdAndId(userId, taskId);
        @NotNull final Project project = projectRepository.findByUserIdAndId(userId, projectId);
        task.setProject(project);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void unbindTaskById(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        @NotNull final Task task = taskRepository.findByUserIdAndId(userId, taskId);
        task.setProject(null);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        taskRepository.deleteByUserIdAndId(userId, projectId);
        projectRepository.deleteByUserIdAndId(userId, projectId);
    }

    @Override
    @Transactional
    public void removeByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final String projectId = projectRepository.findAllByUserId(userId).get(index).getId();
        taskRepository.deleteByUserIdAndId(userId, projectId);
        @NotNull final Project project = projectRepository.findAllByUserId(userId).get(index);
        projectRepository.delete(project);
    }

    @Override
    @Transactional
    public void removeByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final String projectId = projectRepository.findByUserIdAndName(userId, name).getId();
        taskRepository.deleteByUserIdAndId(userId, projectId);
        projectRepository.deleteByUserIdAndName(userId, name);
    }

}
